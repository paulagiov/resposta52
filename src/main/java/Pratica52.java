
import utfpr.ct.dainf.if62c.pratica.*;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica52 {
    public static void main(String[] args) {
       try{
        Equacao2Grau<Integer> e1 = new Equacao2Grau<Integer>(4, 8, 6);
        Equacao2Grau<Integer> e2 = new Equacao2Grau<Integer>(1, -4, -5);
        Equacao2Grau<Integer> e3 = new Equacao2Grau<Integer>(1, 12, 36);
        Equacao2Grau<Integer> e4 = new Equacao2Grau<Integer>(0, 2, 3);
        
        System.out.println("Raízes da E1 " + e1.getRaiz1() + " " + e1.getRaiz2());
        System.out.println("Raízes da E2 " + e2.getRaiz1() + " " + e2.getRaiz2());
        System.out.println("Raízes da E3 " + e3.getRaiz1() + " " + e3.getRaiz2());
        System.out.println("Raízes da E4 " + e4.getRaiz1() + " " + e4.getRaiz2());
        
       }catch(RuntimeException e){
           System.out.println(e.getMessage());
       }
       
    }
}
